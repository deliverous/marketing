IMAGE = deliverous/marketing

JPEG_ORIGINAL = $(shell find content/ -type f -name '*.jpg' ! -name '*__thumbnail-*' )
JPEG_SQUARE = $(patsubst %.jpg, %__thumbnail-square.jpg, $(JPEG_ORIGINAL))
JPEG_WIDE = $(patsubst %.jpg, %__thumbnail-wide.jpg, $(JPEG_ORIGINAL))

%__thumbnail-square.jpg: %.jpg
	convert  -geometry 800x800^ -gravity center -crop 800x800+0+0 "$<" "$@"

%__thumbnail-wide.jpg: %.jpg
	convert "$<" -resize "750>" "$@"

jpeg: $(JPEG_SQUARE) $(JPEG_WIDE)

clean:
	rm -rf ${JPEG_SQUARE}
	find . -name '*thumbnail*' -delete

build:
	docker build --pull -t ${IMAGE} .

test: docker docker-network
	docker run -it --network azae-net registry.gitlab.com/azae/docker/linkchecker linkchecker --anchors --ignore-url="^mailto:" http://deliverous/

	docker stop deliverous
	-docker rm -f deliverous
	-docker network rm deliverous-net

docker: build docker-network
	-docker stop deliverous
	-docker rm -f deliverous
	@docker run --name deliverous -d -p 8080:80 -p 8443:443 --network deliverous-net ${IMAGE}

docker-network:
	-docker network create --driver bridge deliverous-net

hugo: jpeg
	hugo server --buildDrafts --buildFuture --watch --bind 0.0.0.0 --port 8000

docker-dev: build docker-network
	-docker rm -f deliverous
	docker run --rm --name deliverous -p 8000:8000 -p 8080:80 -p 8443:443 --rm -it -v $(shell pwd):/site --entrypoint=/bin/bash --network deliverous-net ${IMAGE}

.PHONY: hugo
